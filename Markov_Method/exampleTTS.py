from gtts import gTTS
from hashlib import sha1
import os
import string

# Functions that makes an mp3 from a message, saves as hash and then plays it.
def ttsHash(message, lang = 'en'):
    str(lang)
    tts = gTTS(text=message, lang=lang)
    a = sha1(message.encode('utf-8')).hexdigest()
    tts.save("sounds/" + a + ".mp3")
    os.system("mpg321 -q sounds/" + a + ".mp3")
